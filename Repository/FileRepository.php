<?php

namespace Lmn\File\Repository;

use Lmn\Core\Lib\Repository\AbstractEventEloquentRepository;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;
use Lmn\Core\Lib\Database\Save\SaveService;
use Lmn\Core\Lib\Database\GeneratorService;
use Lmn\Core\Lib\Facade\Generator;
use Lmn\File\Database\Model\File;

class FileRepository extends AbstractEventEloquentRepository {

    private $generatorService;

    public function __construct(CriteriaService $criteriaService, GeneratorService $generatorService) {
        parent::__construct($criteriaService);
        $this->generatorService = $generatorService;
    }

    public function getModel() {
        return File::class;
    }
}
