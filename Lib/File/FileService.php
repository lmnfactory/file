<?php

namespace Lmn\File\Lib\File;

use Lmn\Core\Lib\Database\GeneratorService;
use Lmn\Core\Lib\Database\Save\SaveService;

use Lmn\File\Repository\FileRepository;

class FileService {

    private $fileRepo;

    public function __construct(FileRepository $fileRepo) {
        $this->fileRepo = $fileRepo;
    }

    private function genPublicId() {
        $generator = \App::make(GeneratorService::class);
        return $generator->uniqueString('subjectprototype', 'public_id');
    }

    public function save($fileData) {
        
    }
}
