<?php

namespace Lmn\File\Database\Validation;

use Lmn\Core\Lib\Model\LaravelValidation;

class FileSizeValidation extends LaravelValidation {

    public function getRules($data) {
        return [
            'file' => 'required|max:10000'
        ];
    }
}
