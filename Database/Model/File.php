<?php

namespace Lmn\File\Database\Model;

use Illuminate\Database\Eloquent\Model;

class File extends Model {

    protected $table = 'file';

    protected $fillable = ['user_id', 'public_id', 'name', 'path', 'extension', 'size', 'system', 'confirmed'];

    protected $hidden = ['lastaccess_at'];
}
