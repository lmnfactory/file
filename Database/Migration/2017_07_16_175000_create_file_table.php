<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->char('public_id', 64);
            $table->char('name', 128);
            $table->char('path', 255);
            $table->char('extension', 20);
            $table->integer('size')->unsigned();
            $table->boolean('system')->default(0);
            $table->boolean('confirmed')->default(0);
            $table->datetime('lastaccess_at')->nullable();
            $table->timestamps();

            $table->index('public_id');
            $table->index('name');
            $table->index('path');
            $table->index('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file');
    }
}
