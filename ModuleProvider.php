<?php

namespace Lmn\File;

use Lmn\Core\Provider\LmnServiceProvider;
use Lmn\Core\Lib\Module\ModuleServiceProvider;
use Illuminate\Support\Facades\Route;

use Lmn\Account\Middleware\SigninRequiredMiddleware;

use Lmn\Core\Lib\Repository\Criteria\CriteriaService;

use Lmn\File\Repository\FileRepository;
use Lmn\File\Repository\Criteria\FileByPublicIdCriteria;

use Lmn\Core\Lib\Model\ValidationService;
use Lmn\File\Database\Validation\FileSizeValidation;

class ModuleProvider implements ModuleServiceProvider{

    public function register(LmnServiceProvider $provider) {
        $app = $provider->getApp();
        
        $app->singleton(FileRepository::class, FileRepository::class);

        $this->registerCommands($provider);
    }

    public function boot(LmnServiceProvider $provider) {
        $app = $provider->getApp();
        
        /** @var CriteriaService $criteriaService */
        $criteriaService = $app->make(CriteriaService::class);
        $criteriaService->add('file.by.publicId', FileByPublicIdCriteria::class);

        /** @var ValidationService $validationService */
        $validationService = \App::make(ValidationService::class);
        $validationService->add('file.size', FileSizeValidation::class);
    }

    public function event(LmnServiceProvider $provider) {

    }

    public function route(LmnServiceProvider $provider) {
        Route::group(['namespace' => 'Lmn\\File\\Controller'], function() {
            Route::any('upload','FileController@upload')->middleware(SigninRequiredMiddleware::class);
            //Route::any('upload','FileController@upload');
            Route::any('/file/{publicId}','FileController@download');
        });
    }

    /**
     * Register all commands with Laravel framework (artisan)
     * @method registerCommands
     */
    private function registerCommands($provider){
        $app = $provider->getApp();
        
    }
}
