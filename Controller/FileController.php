<?php

namespace Lmn\File\Controller;

use Lmn\Core\Lib\Facade\Config;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Lmn\Core\Lib\Response\ResponseService;
use Lmn\Core\Lib\Response\ResponseMessage;
use Lmn\Account\Lib\Auth\CurrentUser;
use Lmn\Core\Lib\Model\ValidationService;

use Lmn\File\Repository\FileRepository;

class FileController extends Controller {

    public function __construct() {

    }

    public function upload(Request $request, FileRepository $fileRepo, CurrentUser $currentUser, ValidationService $validationService) {
        $subfolder = time() % 1000;
        $uploadedFile = $request->file('file');
        $filePath = $uploadedFile->store('uploads/' . $subfolder);

        $validationService->validate(['file' => $uploadedFile], 'file.size');

        $file = $fileRepo->clear()
            ->create([
                'user_id' => $currentUser->getId(),
                'public_id' => basename($filePath),
                'size' => $uploadedFile->getClientSize(),
                'extension' => $uploadedFile->extension(),
                'path' => $filePath,
                'name' => $uploadedFile->getClientOriginalName()
            ]);

        return $file->id;
    }

    public function download($filePid, Request $request, FileRepository $fileRepo, CurrentUser $currentUser) {

        $file = $fileRepo->clear()
            ->criteria('file.by.publicId', ['publicId' => $filePid])
            ->get();

        return response()->download(storage_path("app/" . $file->path));
    }
}
